
# Creating a Rest API with JWT authentication and role based authorization using TypeScript…

    npm install -g typeorm

    typeorm init --name jwt-express-typeorm --database sqlite --express

    npm install

    npm install -s helmet cors jsonwebtoken bcryptjs class-validator ts-node-dev

    npm install -s @types/bcryptjs @types/body-parser @types/cors @types/helmet @types/jsonwebtoken

    http://localhost:3000/auth/login

    typeorm migration:create -n CreateAdminUser

    npm start

    npm run migration:run
